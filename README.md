## Learn Browsers Extensions
###by mcrdy455
*This is still work in progress, it will contantly be improoved*

### Contents 
1.  <a href="#introduction">Introduction</a><br />
1.1. <a href="#links">Useful links</a><br />
1.1.1. <a href="#tutorials">Tutorials</a><br />
1.1.2. <a href="#docs">Documentation</a><br />
1.1.3. <a href="#cheat">Cheatsheets</a><br />
1.2. Useful code<br />
1.2.1. manifest.json<br />
1.2.2. JS functions<br />
2. Getting Started.<br />
2.1. Setting up the manifest.<br />
2.2. Developing process <br />
2.3. Things to keep in mind.<br />
2.3.1. when picking an idea<br />
2.3.2. when developing<br />
3. Using it.<br />
3.1. Developer mode (user unfriendly)<br />
3.2. Firefox<br />
3.3. Publishing<br />
4. Notes<br />
5. 

## 1. <a name="introduction"></a>Introduction
### <a name="links"></a>Useful links
##### <a name="tutorials"></a>Tutorials<br />
[The coding train chrome extensions and bookmarklets tutorials playlist](https://www.youtube.com/watch?v=hkOTAmmuv_4&list=PLRqwX-V7Uu6bL9VOMT65ahNEri9uqLWfS&index=2&t=0s)
* This is a pretty good tutorial, that helped me start writing extensions. No other tutorial was needed after that.
* [Episode 1](https://www.youtube.com/watch?v=hkOTAmmuv_4&list=PLRqwX-V7Uu6bL9VOMT65ahNEri9uqLWfS&index=1) is an introduction.
* [Episode 2](https://www.youtube.com/watch?v=DloHqUfPbJc&list=PLRqwX-V7Uu6bL9VOMT65ahNEri9uqLWfS&index=2) is about bookmarklets.
* [Episode 3](https://www.youtube.com/watch?v=9Tl3OmwrSaM&list=PLRqwX-V7Uu6bL9VOMT65ahNEri9uqLWfS&index=3) is where the coding starts.
* [Episode 4](https://www.youtube.com/watch?v=ew9ut7ixIlI&list=PLRqwX-V7Uu6bL9VOMT65ahNEri9uqLWfS&index=4) is when I started building alone.
* I advice watching until the end to learn some more of the extensions basics, **unless** you have an **idea or a goal** that **you desire to fulfil**.

##### <a name="docs"></a>Documentation
* [Chrome Official Docs](https://developer.chrome.com/extensions/)<br /> 
* [Mozilla Official Docs](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions)<br />
In some cases chrome extensions wont work on mozilla and other mozilla based browsers. The reasons are mostly additional permissions you need to include in your manifest.

##### <a name="cheat"></a>Cheatsheets
Here are some useful documents I use during my developement process: <br />
* [XPath cheatsheet](https://gist.github.com/LeCoupa/8c305ec8c713aad07b14): Using XPath selectors rather than the traditional CSS query selectors, helps your extension survive a little bit longer. The main problem with extensions and selectors, is that if the targeted website, desides to redesign, this will most likely break your code.<br />
* [JS cheatsheet](https://github.com/LeCoupa/awesome-cheatsheets/blob/master/languages/javascript.js): Not something I ever used, but here are some usefull Objects and Arrays functions, some of which may save you 3-4 lines of code and make your code more readable.

### <a name="code"></a>Useful code
##### manifest.json
The only requirement for creating an extension is the manifest.json file, which contains important information about it.<br />
Every single JSON property in listed in [this docs page](https://developer.chrome.com/extensions/manifest). This documentation page could be used for finding ideas about what you can do and what you shouldn't do, when programming an extension<br />

The most common manifest.json file looks like this
```json
{
  "manifest_version": 2, //(required) version 1 is not supported since 2014 and Chrome 18
  "name": "Template", //(required) 
  //(optional) Important part of the extension. Used to require extra functionality to use in the extension.
  // NEVER add permissions, that are not required for running the extension.
  "permissions": [
    "https: //*.extensionsworld.com/*", //host permissions, required mostly for firefox
    "storage" //used to create own extensions storage,
  ],
  "version": "1.0.0", //(required) used for automatically updating the extension.
  //(optional) background scripts are a set of scripts that will start running on browser startup.
  //BG scripts can comunicate with content scripts or work individually, while the browser is running.
  //BG scripts also have access to all extensions specific functionality.
  "background": {
    "scripts": [
      "js/utils.js",
      "js/background.js"
    ]
  },
  //(optional) content scripts are JS files that will run on loading a page and execute inside the page, like a standard JS file.
  "content_scripts": [ 
    {
    //the folling array hold information about where the set of JS files will execute. Check "Match patterns" to learn more
    //Match Patters docs: https://developer.chrome.com/extensions/match_patterns
      "matches": [
        "https://*.example.com/*",
      ],
      "js": [
        "js/utils.js",
        "js/index.js"
      ]
    }
  ],
  //(optional) icons, which will be displayed in chrome store, extensions list page and favicons for extensions specific html pages 
  "icons": {
    "128": "icon128.png",
    "64": "icon64.png"
  },
  //(optional) "broswer_action" (or the simplified "page_action") adds an icon in the main Browser toolbar, to the right of the address bar.
  // more can be read here: https://developer.chrome.com/extensions/browserAction
  "browser_action": {
    //this icon will be used in the Browser toolbar only
    "default_icon": {
      "128": "icon128.png",
      "64": "icon64.png"
    },
    // Tooltip text when hovering over the icon with the mouse
    "default_title": "Template",
    // HTML file to display as s popup after clicking the icon.
    "default_popup": "html/index.html"
  }
}
```

##### JavaScript functions.
[JQuery](https://jquery.com/) is a good tool for beginers. It can be added as a "content script"<br />
[Extra Dev Scripts](https://gitlab.com/vladislav.kostov432/extra-dev-scripts) is a chrome developer only extension that injects some scripts to the console, simply by clicking the browser_action icon. Its useful for coding and testing some of the functionality of a content script before moving it to the JS file. Its probably one of the most used extensions by me while developing. Feel free to add any functions you use regulary.<br />

